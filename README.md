<!-- [![](doc/logo_small.png)](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis) -->

<img src="doc/logo_small.png" width="100">





# HexPlot

[![Build Status](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis/badges/master/pipeline.svg)](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis/commits/master)

### A Generic Geometrical Plotting Framework

This is a small program to display data for geometrical shapes. It can plot any values in a tab separated data file on any geometry specified in a geometry file.

It is intended to be lightweight, flexible and plug-and-play.

Feel free to give feedback, report issues or request changes. Contributions and merge requests are always welcome!




## Installation:

### Installation on Linux:

#### Dependencies
* [ROOT](https://root.cern.ch/building-root) (tested from ROOT5 on)
* [gcc](https://gcc.gnu.org/)

On SLC6 PCs or lxplus try to source this before:
```
$ . /cvmfs/sft.cern.ch/lcg/releases/gcc/4.9.3/x86_64-slc6-gcc49-opt/setup.sh
$ . /cvmfs/sft.cern.ch/lcg/releases/ROOT/6.06.06-a2c9d/x86_64-slc6-gcc49-opt/bin/thisroot.sh
```

#### Compilation

HexPlot uses CMake to configure its build system. In order to compile the program, run

```
$ mkdir build && cd build/
$ cmake ..
$ make
# If an installation is desired:
$ make install
$ ../bin/HexPlot  # execute help
```

The install path by default is the project's source directory, binaries are placed in the `bin/` and libraries in the `lib/` subdirectories.
The installation folder can be changed using e.g.

```
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local/
```

To update the doxygen documentation make sure you have a doxygen version later than 1.8.3 and execute
```
make doc
```
### Installation on Windows:

The compiled windows executable is delivered. ROOT is required for execution. Install Root and add it to the Windows path environmental variable. For convenience an auto-update PDF viewer is recommended (e.g. [Sumatra PDF](https://www.sumatrapdfreader.org/free-pdf-reader.html)).

#### Dependencies
* [ROOT v5.34.36](https://root.cern.ch/content/release-53436) (latest windows compatible version)
* [Visual Studio 2013](https://www.visualstudio.com/vs/older-downloads/) (only for compilation)

#### Compilation

HexPlot can only be compiled on Windows with Visual Studio 2013. The most important steps for compilation are (taken from a VS2013 Update 4 setup):

* open Visual Studio, create a new empty Visual C++ project and name it `HexPlot`
* add all header files and source files by dragging each `.cxx` and `.h` file into the Solution Explorer. It is important not to include ```\utils\test_hash.cxx``` in the source files, otherwise an error is thrown at the linking stage during the build!
* right click on the project in the Solution Explorer *HexPlot -> Properties ->*
  * *C++ -> Additional Include Directories*: add the ROOT includes, usually in `C:\root_v5.34.36\include`
  * *C++ -> Advanced -> Forced Include Files*: add `C:\root_v5.34.36\include\w32pragma.h`
  * *Linker -> Input -> Additional Dependencies*: add ROOT `.lib` positions: `C:\root_v5.34.36\lib\*.lib`
  * *Linker -> System -> Subsystem*: set to Console
* optional: set the output directory for the executable: *DEBUG -> HexPlot properties -> Config. Prop. -> Output Directory*: add your favorite folder. The default folder is usually `Documents\Visual Studio 2013\Projects\HexPlot\HexPlot\Debug`
* Build the solution

Check [this webpage](https://twiki.cern.ch/twiki/bin/view/Sandbox/ROOTandVisualStudio) for more and detailed instructions on how to build a ROOT project using Visual Studio..





## Documentation:
The help will show you the first steps:
```
$ HexPlot.exe --help # Windows
$ HexPlot --help # Linux
```
To familiarize yourself with the
program, make sure to study all examples via
```
$ ./HexPlot --examples
$ ./HexPlot --example [number]  # execute specific example
$ ./HexPlot --example [number] [--additional option]  # add flags to alter example
```
and modify the example data files in the `examples` and the geometry files in the `geo` directory. The documentation of the source code is called via
```
$ ./HexPlot --doxygen
```
Finally, have a look at this [presentation](http://amaier.web.cern.ch/amaier/HexPlot.pdf) for a quick overview!








## Syntax
This is a short overview of the expected syntax. Consult the help and the example files referenced in the examples for more information.

### Data file syntax:
The data files are tab or space separated columns following the TTree:ReadFile() format:
```
#padNumber     selector     value
1              0            0
2              0            1
```
The padnumber links the value to the pad specified in the geometry file. The selector is usually a voltage for IV and CV curves, but serves in general to distinguish between different entries for the same pad. The value is the actual value to be displayed. For formats deviating from the above default, the flag `--if` has to be specified. As an example, this data file layout:
```
#padNumber     timestamp     temperature     current     warning     voltage
1              2:12          29.405          1.102e-9    0           500
2              2:13          29.403          2.237e-9    0           500
```
is matched with this flag, if one wants to show the recorded current for 500 V:
```
--if PADNUM:none:none:VALUE:none:SELECTOR
```

### Geometry file syntax:
The geometry files are tab or space separated columns with the following information:
```
#padNumber     xPosition     yPosition     cellType     parameterString
1              0             0             0            SIZE:1
2              0             1             15           SIZE:1,ROTATE:45
```
The parameter string has to specify at least a 'SIZE:value'. It cannot contain whitespace chars and has to be of the form 'key:value' or 'key', separated by ','. For example:
```
SIZE:0.5,X_STRETCH:2,ROTATE:-30,INVERT,X_SHEAR:0.5,Y_CROP:0.3
```
The commands are executed in order. Strings passed with the TEXT option cannot contain whitespace, use '~' instead. This will be replaced by a space. For everything else, they follow the Root TLatex conventions. More available parameters can be looked up in `src/geo_plot.cxx` or in `geo/example_shapes.txt`.

### Map file syntax
Map files provide an optional additional layer of mapping between the pad numbers in the data and in the geometry file, in whitespace separated rows. If no map file is specified with `-m`, identity is assumed. This is the syntax to link data of pad 1 and 2 into the geometry position of pad 67 and 66, respectively.
```
#data     geometry
1         67
2         66
```





## Development of HexPlot

### Contributing
Feel free to give feedback, report issues or request changes. Contributions and merge requests are always welcome!

If you have any doubt about the best way to implement new functionality or how to split it up, please open an issue with the discussion tag on the issue tracker. Also please feel free to open a incomplete merge request early on with the WIP (work-in-progress) label to allow for early discussion.

Base your branch on the protected `develop` branch. If you have implemented new examples, remember to update the ctest files with `utils/run_multiple.py --alltestfiles`.

### Continuous Integration

HexPlot uses the GiLab CI in order to ensure code quality and functionality. Every push to the repository initializes the execution of a pipeline with the following targets:

* Compilation of the source code under SLC6, CentOS7 and Mac OSX
* Compilation of the source code with GGC (SLC6, Centos7) and Clang (all three)
* Execution of `clang-format` to check if the code satisfies the formatting rules defined in the [.clang-format](.clang-format) file (see below)
* Building of the Doxygen code reference. The result of this can be downloaded as build artifact from the respective CI job for three hours after the build.

### Code Formatting and Linting

HexPlot comes with a defined style of coding (defining things like the indentation width and the position of brackets) following the LLVM style. All definitions can be found in the [.clang-format](.clang-format) file. For the functionality described below to work, `clang`, `clang-tidy` and `clang-format` verions of at least 4.0 are required.

In order to ease the use of these, additional CMake targets are provided:

* `make check-format` simply checks if the code adheres to the coding style and will throw an error if not. This target is mostly meant for the CI as no automatic server-side corrections can be made.
* `make format` will run `clang-format` with the project's style on all source and header files of the project and will correct (possible) coding style errors such as indentations and spacing. This should be done before every commit, unless the pre-commit git hook is activated (see below).

These targets are only available if the `clang-format` executable is found on your system. Code which does not follow the coding style will not be merged to the repository.

In addition to source code style, source code linting can help to improve code quality. If the clang compiler is used and the `clang-tidy` executable is found on your system, the following additional make targets are available:

* `make check-lint` works just as the `check-format` target and checks for errors produced by the linting process.
* `make lint` will automatically correct errors found by the linting program, such as missing brackets around single statements after if, e.g.

   ```cpp
   if(myboolen) std::cout << "It's true!";
   ```

   would become

   ```cpp
   if(myboolean) {
       std::cout << "It's true!";
   }
   ```

In order to switch your compiler to `clang`, the build directory has to be cleaned and the following environment variables have to be set before invoking cmake:

```bash
export CXX=clang++
export CC=clang
cmake ..
```


### Git Hooks

A git pre-commit hook is provided in the `/utils/git-hooks` directory. The [hook](utils/git-hooks/pre-commit-clang-format-hook) will execute `clang-format` and ensure compatible coding style (see above). It is invoked by git automatically when committing new code to the repository.

The hook has to be installed once in every clone of the repository by executing the [shell script](utils/git-hooks/install-hooks.sh) provided in the git-hooks directory.


### Contributors
HexPlot has been developed by:

* Andreas Alexander Maier, CERN, @amaier

and is maintained by:

* Erica Brondolin, CERN, @ericabro

The following authors, in alphabetical order, have contributed to the project:

* Thomas Connor Hodson, University of Cambridge, @thodson
* Florian Michael Pitters, CERN, @fpipper
* Pedro Da Silva, CERN, @psilva
* Simon Spannagel, CERN, @simonspa
