# HexPlot Documentation

![](logo_small.png "HexPlot Logo")

This is a simple documentation of the HexPlot code. It can plot any values in a tab separated data file on any geometry specified in a geometry file. Please consult the Readme for additional information. The git repository can be found at (https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis). Feel free to give feedback there, report issues or request changes. Contributions and merge requests are always welcome!

![](example.png "Example figure with arbitrary values and highlighted 'jumper' cells on a Hamamatsu 6 inch sensor for the CMS HGCAL project")
