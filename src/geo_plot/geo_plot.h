/**
        @file geo_plot.h
        @brief Plots values according to a given geometry in several gemoetrical
  shapes
  @author Andreas Alexander Maier
*/

#ifndef GEO_PLOT_H
#define GEO_PLOT_H

#include <cmath>
#include <utility>

#include "TCanvas.h"
#include "TH2F.h"
#include "TImage.h"
#include "TList.h"
#include "TMath.h"
#include "TPaletteAxis.h"
#include "TPave.h"
#include "TPaveText.h"
#include "TPolyLine.h"
#include "TStyle.h"
#include "TTree.h"
#include "TVector2.h"

#include "../utils/root_utils.h"

const int MAX_PAD_NUMBER = 100000;
const int MAX_INTERCELL_NUMBER = 10;
const int MAX_CELL_CORNER_NUMBER = 10000;

/**
        @brief A class for plotting values in a given geometrical order

        This class can be used to plot any structure of any geometrical shape
        and fill it with values.
*/
class geo_plot {
 public:
  /// Set the value of an individual cell
  void fill(int bin, double value);
  /// Set the values of cells from a histogram
  void fill(TH1F* h_fill);
  /**
    @brief Set values for inter-cell display

    Every cell has possible inter-cells at each of its edges, with indices
    starting at 0. This function fills one inter-cell value with index
    interIdx for every cell with values taken from the histogram.

    @param h_fill histogram with one bin for each cell
  */
  void fill_inter_cell(TH1F* h_fill, int interIdx);
  /**
    @brief Set values for inter-cell display

    Fills inter-cells with automatically incremented index for each cell,
    starting at 0.

    @param interIdx index of inter-cell to be filled for each cell
  */
  void fill_inter_cell(TH1F* h_fill) { fill_inter_cell(h_fill, nInterCells); };
  /**
  @brief Draw the filled values in their shapes

  This function initiates a series of other helper functions to draw
  all required objects.
  */
  void draw();
  /// Sets the title to be displayed on the z-axis
  void set_z_title(std::string s) { zTitle = std::move(s); };
  /// Skip bins with content 0 from display
  void set_zero_suppress(bool b) { zeroSuppress = b; };
  /// Set range of z-axis
  void set_z_range(double min, double max) {
    zMin = min;
    zMax = max;
  };
  /// Set info text appearing in the top left corner of the plot
  void set_top_left_info(std::string s) {
    topLeftInfo = std::move(s);
    infoOption += "TOPLEFT";
  };
  /// Set info text appearing in the top right corner of the plot
  void set_top_right_info(std::string s) {
    topRightInfo = std::move(s);
    infoOption += "TOPRIGHT";
  };
  /// Set info text appearing in the lower left corner of the plot
  void set_low_left_info(std::string s) {
    lowLeftInfo = std::move(s);
    infoOption += "LOWLEFT";
  };
  /// Set info text appearing in the lower right corner of the plot
  void set_low_right_info(std::string s) {
    lowRightInfo = std::move(s);
    infoOption += "LOWRIGHT";
  };
  /// Print out debug information
  void set_verbose(int v) { verbose = v; };
  /// Specify file to save the plot. Default is hex.png.
  void set_output_file(std::string s) { outputFile = std::move(s); };
  /// Scale the values of a cell type for display
  void set_pad_type_scale(int padType, double scale) { padTypeScale[padType] = scale; };
  /// Scale the values of a cell type for display
  void set_pad_scale(int pad_idx, double scale) { padScale[pad_idx] = scale; };
  /// Set color palette
  void set_color_palette(int pn) { paletteNumber = pn; };
  /// Set text color
  void set_text_color(int tc) { textColor = tc; };
  /// Toggle no axis display
  void set_no_axis(bool b) { noAxis = b; };
  /// Control amount of text printed on plot
  void set_info(bool b) { infoOption = b ? "ALL" : "NONE"; };
  /**
    @brief Control amount of text printed on plot

    Selectively enable regions of the plot where text is displayed.
    The string can be any combination of 'TOPLEFT', 'TOPRIGHT',
    'LOWLEFT', 'LOWRIGHT'. E.g. 'TOPLEFTLOWRIGHT'.
  */
  void set_info(std::string s) { infoOption = std::move(s); };
  /// Specify geometry of the layout
  void set_geo_file(std::string s) { geoFile = std::move(s); };
  /// Specify number to display in each cell
  void set_pad_number_option(int option) { padNumOpt = option; };
  /// Set number of digits after the point for pad content display
  void set_pad_value_precision(int i) { valPrecision = i; };
  /// Print out hashed info on result
  void set_test_info(bool p) { testInfo = p; };
  /// Select a special cell to be highlighted
  void highlight_pad(int i) { highlightPads.push_back(i); };
  /// Set log scale on the Z axis
  void set_use_logy(bool p = true) { useLogVal = p; };

  /**
    @brief Select how to highlight a cell

    Can be concatenated as needed, 'dottedsmallnumbers'
  */
  void highlight_pad_option(std::string s = "dotted") { treatOtherPads = std::move(s); };
  /// Draw all cells in color (following TColor code)
  void force_pad_colors(int color) { overwritePadColors = color; };
  /// Return type number of a cell
  int get_pad_type(int padNum);
  /// Return vector of pad types
  std::vector<int> get_pad_type_vec();
  /// Get surface area of a pad (not functional at the moment)
  double get_pad_surface(int padType);
  /// Return vector of pad type surfaces
  std::vector<double> get_pad_surface_vec();
  /// Get type description of a cell
  std::string get_pad_type_string(size_t);
  /// Return vector of pad type descriptions
  std::vector<std::string> get_pad_type_string_vec();
  /// Get type description of a type
  std::string get_type_string(int);
  /// Reset contents and selected other settings to start a new plot
  void reset();
  /// Constructor setting standard settings
  geo_plot();
  /// Standard destructor
  ~geo_plot();

 private:
  const static int N_ADD_PARS = 10;
  const int DEFAULT_COLOR = 18;

  int nPads;
  int nNegativePadNums;
  int valPrecision;
  int padNumOpt;
  int paletteNumber;
  int nInterCells;
  int overwritePadColors;
  int textColor;
  int verbose;
  std::string zTitle;
  bool useLogVal;
  std::string outputFile;
  std::string topLeftInfo;
  std::string topRightInfo;
  std::string lowLeftInfo;
  std::string lowRightInfo;
  std::string geoFile;
  std::string infoOption;
  bool noAxis;
  bool geoFileIsRead;
  bool drawInterPadValues;
  bool testInfo;
  bool zeroBased;
  bool zeroSuppress;
  std::string treatOtherPads;
  TH2F* h_vals;
  TH1* h_colors;
  TH2F* h_vals_inter[MAX_INTERCELL_NUMBER];
  TH1* h_colors_inter[MAX_INTERCELL_NUMBER];
  TTree* geoTree;
  double padTypeScale[MAX_PAD_NUMBER];
  double padScale[MAX_PAD_NUMBER];
  double zMin;
  double zMax;
  std::vector<int> padNumbers;
  std::vector<double> padXPositions;
  std::vector<double> padYPositions;
  std::vector<int> padTypes;
  std::vector<std::string> padTrafos;
  std::vector<int> highlightPads;
  void draw_white_box();

  // clang-format off
  TPolyLine* draw_polygram(double x, double y, const std::string& trafo, int corners, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_tetragram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_pentagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_hexagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_heptagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_octagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);

  TPolyLine* draw_equipoly(double x, double y, const std::string& trafo, int corners, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_triangle(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_square(double x, double y, std::string trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_pent(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hept(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_oct(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_circle(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_hex_cornercut(double x, double y, std::string trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_cornercut_remove_edge(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_half(double x, double y, std::string trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);

  TPolyLine* draw_equipoly_ring(double x, double y, const std::string& trafo, int corners, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_triangle_ring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_square_ring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_pent_ring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_ring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hept_ring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_oct_ring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_circle_ring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);

  TPolyLine* draw_bar_polygram(double x, double y, const std::string& trafo, int bars, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_bar_trigram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_plus(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_bar_pentagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_bar_hexagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_bar_heptagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_bar_octagram(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);

  TPolyLine* draw_hex_otherhalf(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_smallotherhalf(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_diamond(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_jumper(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_mousebite(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_cornercut_long(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_cornercut_long_cut(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);
  TPolyLine* draw_hex_cornercut_long_remove_edge(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001, int interCellStyle = 1001);

  TPolyLine* draw_guardring(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_arrow(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_flash(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_person(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_heart(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_flower(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  TPolyLine* draw_moon(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  // clang-format on

  void draw_intercell(double x, double y, const std::string& trafo, int col, int style = 1001);
  void draw_intercells(TPolyLine* pline, int npar, double side, int colorArray[2 + MAX_INTERCELL_NUMBER],
                       int interCellStyle, int starthere = 0);
  TPolyLine* create_poly(int n, double x, double y, double xvals[], double yvals[], const std::string& trafo,
                         int colorArray[2 + MAX_INTERCELL_NUMBER], int style = 1001);
  /// Draw the number on top of a cell
  void draw_number(double x, double y, int color, int i);
  /// Return true if this pad has been set as special
  bool is_special_pad(int padNumber);
  /// Draw all cells found in geometry file
  void draw_polygons();
  /// Draw the information text in the corners
  void draw_info();
  /// Draw the logo in the upper left corner
  void draw_logo();
  /// Fill the information from the geometry file
  void fill_geo_info();
  /// Determine z-axis range
  void adjust_z_range();
  /// Get the absolute bin number corresponding to a cell
  int get_bin_number(int bin_number_no_overflow);
  /// Analyzes additional parameter strings
  std::string extract_additional_parameters(const std::string& addPars);
  /// Get first appearing single value of a transformation parameter
  std::string get_par_val(const std::string& padTrafo, const std::string& parString);
  /// like get_par_val() but with conversion to double
  double get_par_val_double(const std::string& padTrafo, const std::string& parString);
  /// like get_par_val_double() but sums all appearances
  double get_total_sum_par_val_double(const std::string& padTrafo, const std::string& parString);
  /// Replace a value in the parameter string
  std::string replace_par_val(const std::string& padTrafo, const std::string& parString, const std::string& newVal);
};

#endif
