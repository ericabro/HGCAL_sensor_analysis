/**
        @file HexPlot.cxx
        @brief This is the main file of HexPlot.
  @author Andreas Alexander Maier
*/
#include <iostream>
#include <string>

#include "hex_plotter.h"

int main(int argc, char **argv) {
  std::cout << "Executing:" << std::endl;
  for (int i = 0; i < argc; i++) {
    std::cout << argv[i] << " ";
  }
  std::cout << std::endl;

  // clang-format off
  auto *hexplot = new hex_plotter(argc, argv);
  if (hexplot->parse_input() != 0) {return 1; }
  if (hexplot->print_help() != 0) {return 0; }
  if (hexplot->print_examples() != 0) {return 0; }
  if (hexplot->print_version() != 0) {return 0; }
  if (hexplot->show_doxygen() != 0) {return 0; }
  if (hexplot->print_defaults() != 0) {return 0; }
  if (hexplot->run() != 0) {return 0; }
  // clang-format on

  if (hexplot->get_verbose() != 0) {
    std::cout << "Executed:" << std::endl;
    for (int i = 0; i < argc; i++) {
      std::cout << argv[i] << " ";
    }
    std::cout << std::endl;
  }

  return 0;
}
